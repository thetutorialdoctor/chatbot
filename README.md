# ChatBot

Chatbot from [this](https://www.youtube.com/watch?v=CkkjXTER2KE) video

Using it to integrate into an [AI social media app](https://designanddevelopblog.vercel.app/apps/8) I'm making using Flask.

If you are interested in getting updates on the AI social media app (source code will be released at some point), subscribe to me on [Patreon](https://www.patreon.com/posts/building-ai-app-91515212?utm_medium=clipboard_copy&utm_source=copyLink&utm_campaign=postshare_creator&utm_content=join_link)
